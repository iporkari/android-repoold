package it.unimore.ing.homeplugs;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;

public class HPsListAdapters extends BaseAdapter implements ListAdapter {
    private ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
    private Context context;


    public HPsListAdapters(HashSet<BluetoothDevice> hash, Context context) {
        this.list.addAll(hash);
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int pos) {
        return list.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
        //just return 0 if your list items do not have an Id variable.
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        // view == null --> ovvero la ListView sta compilando la PRIMA riga della list
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_layout, null);
        //Handle TextView and display string from your list
        TextView listItemText = (TextView) view.findViewById(R.id.list_item);
        listItemText.setText(list.get(position).getName());

        //Handle buttons and add onClickListeners
        ImageButton infoBtn = (ImageButton) view.findViewById(R.id.info_button);
        ImageButton connectBt = (ImageButton) view.findViewById(R.id.bt_connect);
        ImageButton addFavBt = (ImageButton) view.findViewById(R.id.bt_add_fav);
        ImageButton delFavBT = (ImageButton) view.findViewById(R.id.bt_del_fav);

        infoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do something
                //list.remove(position); //or some other task
                //notifyDataSetChanged();
            }
        });

        connectBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //do something
                //list.remove(position); //or some other task
                notifyDataSetChanged();
            }
        });

        addFavBt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.favDevices_hash.add(list.get(position));
                notifyDataSetChanged();
            }
        });

        delFavBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.favDevices_hash.remove(list.get(position));
                notifyDataSetChanged();
            }
        });
        return view;
    }
}
