package it.unimore.ing.homeplugs;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    protected static HashSet<BluetoothDevice> pairedDevices_hash;     // set dei dispositivi associati
    protected static HashSet<BluetoothDevice> favDevices_hash;        // set dei dispoitivi preferiti
    public BluetoothDevice bt;
    // variables
    BluetoothAdapter btAdapter;                     // adattatore bluetooth del dispositivo
    HPsListAdapters listAdapterP;                    // necessaria per implementare la ListView con l'elenco dei dispositivi
    HPsListAdapters listAdapterF;
    ListView lv_pair;                               // list view dispositivi associati
    ListView lv_fav;                                // list view dispositivi preferiti
    private ImageButton bt_add_fav;
    private ImageButton bt_del_fav;
    private String filename = "btsaving";
    private FileOutputStream OUT_btsaving;
    private FileInputStream IN_btsaving;

    // costruttore del main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);


        try {
            OUT_btsaving = openFileOutput(filename, MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            IN_btsaving = openFileInput(filename);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // chiamata al metodo di inizializzazione
        init();

        // controllo compatibilità del sistema con il bluetooth
        if (btAdapter != null) {
            //Snackbar.make(findViewById(R.id.activity_main), R.string.no_bluetooth, Snackbar.LENGTH_LONG).show();
            //finish();
        } else {
            if (!btAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }
        }
        refresh();
    }

    // metodo di compilazione della lista dei dispositivi salvabili
    private void CompilePairList() {
        Set<BluetoothDevice> temp_set = btAdapter.getBondedDevices();

        for (BluetoothDevice bt : temp_set) {
            pairedDevices_hash.add(bt);
        }
        pairedDevices_hash.removeAll(favDevices_hash);
        listAdapterP = new HPsListAdapters(pairedDevices_hash, this);
        lv_pair.setAdapter(listAdapterP);

        //Snackbar.make(findViewById(R.id.myCoordinator), R.string.ShowPairDev, Snackbar.LENGTH_LONG).show();
    }

    // metodo di compilazione della lista dei dispositivi salvati
    private void CompileFavList() {
        listAdapterF = new HPsListAdapters(favDevices_hash, this);
        lv_fav.setAdapter(listAdapterF);
    }

    // metodo di inizializzazione dell'activity
    private void init() {
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        lv_pair = (ListView) findViewById(R.id.list_pair_dev);
        lv_fav = (ListView) findViewById(R.id.list_fav_dev);
        bt_add_fav = (ImageButton) findViewById(R.id.bt_add_fav);
        bt_del_fav = (ImageButton) findViewById(R.id.bt_del_fav);

        favDevices_hash = new HashSet<>();
        pairedDevices_hash = new HashSet<>();

        readFile();
    }

    // metodo pulsante Bluetooth —> impostazioni di sistema
    public void BTsetting(View v) {
        startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
    }

    // creazione menu a scomparsa toolbar
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.hp_menu, menu);
        return true;
    }

    // gestore menù
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(Settings.ACTION_BLUETOOTH_SETTINGS));
                return true;

            case R.id.refresh: {
                // refresha il layout
                refresh();
                return true;
            }

            case R.id.siren:
                // attiva modalità antifurto

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    private void refresh() {
        CompileFavList();
        CompilePairList();
        writeFile();
    }

    private void readFile() {
        try {
            ObjectInputStream IN_saving_OBJ = new ObjectInputStream(IN_btsaving);
            favDevices_hash = (HashSet<BluetoothDevice>) IN_saving_OBJ.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeFile() {
        try {
            ObjectOutputStream OUT_saving_OBJ = new ObjectOutputStream(OUT_btsaving);
            OUT_saving_OBJ.writeObject(favDevices_hash);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}










